import React from 'react';
import IntroContainer from '../IntroContainer/IntroContainer';
import { Typography, makeStyles, Theme } from '@material-ui/core';

import Logo from './session-ended-logo.png';

const useStyles = makeStyles((theme: Theme) => ({
  headingContainer: {
    marginTop: '1.5em',
    marginBottom: '1.5em',
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    marginTop: '1em',
    marginBottom: '1em',
    fontWeight: 600,
  },
  content: {
    marginBottom: '1em',
    fontWeight: 400,
    textAlign: 'justify',
  },
  logo: {
    width: '50px',
    height: '50px',
    marginRight: '5px',
  },
}));

export default function ExpirationScreen() {
  const classes = useStyles();
  return (
    <IntroContainer containerWidth='600px' contentWidth='300px'>
      <div className={classes.headingContainer}>
        <img src={Logo} alt='Session Expired Logo' className={classes.logo} />
        <Typography variant='h5' className={classes.heading}>
          Expired Session
        </Typography>
      </div>
      <Typography className={classes.content}>
        It appears as if the session has expired. An Agent will be in touch to schedule another time for the interview
        and a new link will be sent through text message (SMS), or please call&nbsp;
        <a href='tel:833-753-0909'>833-753-0909</a>.
      </Typography>
    </IntroContainer>
  );
}
