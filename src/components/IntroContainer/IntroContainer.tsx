import React from 'react';
import { makeStyles, Theme } from '@material-ui/core';
// import Swoosh from './swoosh';
// import VideoLogo from './VideoLogo';
// import { useAppState } from '../../state';
// import UserMenu from './UserMenu/UserMenu';
// import { useLocation } from 'react-router-dom';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

import LogoVertical from './texas_logo_v.png';
import LogoHorizontal from './texas_logo_h.jpg';

const useStyles = makeStyles((theme: Theme) => ({
  background: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    background: '#fff',
    height: '100%',
  },
  container: {
    position: 'relative',
    flex: '1',
  },
  innerContainer: {
    display: 'flex',
    width: '888px',
    height: '379px',
    borderRadius: '8px',
    boxShadow: '0px 2px 4px 0px rgba(40, 42, 43, 0.3)',
    overflow: 'hidden',
    position: 'relative',
    margin: 'auto',
    [theme.breakpoints.down('sm')]: {
      display: 'block',
      height: 'auto',
      width: 'calc(100% - 40px)',
      margin: 'auto',
      maxWidth: '400px',
    },
  },
  swooshContainer: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    backgroundSize: 'cover',
    width: '296px',
    borderRight: '1px solid rgba(40, 42, 43, 0.3)',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      height: '100px',
      backgroundPositionY: '140px',
      borderRight: 0,
      borderBottom: '1px solid rgba(40, 42, 43, 0.3)',
    },
  },
  logoContainer: {
    position: 'absolute',
    width: '210px',
    textAlign: 'center',
    [theme.breakpoints.down('sm')]: {
      display: 'flex',
      alignItems: 'center',
      width: '90%',
      textAlign: 'initial',
      '& svg': {
        height: '64px',
      },
    },
  },
  logo: {
    width: '100%',
  },
  twilioLogo: {
    position: 'absolute',
    top: 0,
    left: 0,
    margin: '20px',
  },
  content: {
    background: 'white',
    width: '100%',
    padding: '2em',
    flex: 1,
    [theme.breakpoints.down('sm')]: {
      padding: '2em',
    },
  },
  title: {
    color: 'white',
    margin: '1em 0 0',
    [theme.breakpoints.down('sm')]: {
      margin: 0,
      fontSize: '1.1rem',
    },
  },
  subContentContainer: {
    position: 'absolute',
    marginTop: '1em',
    width: '100%',
  },
}));

interface IntroContainerProps {
  children: React.ReactNode;
  subContent?: React.ReactNode;
  containerWidth?: any;
  contentWidth?: any;
}

const IntroContainer = (props: IntroContainerProps) => {
  const classes = useStyles();
  // const { user } = useAppState();
  // const location = useLocation();
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down('sm'));

  let containerWidth, contentWidth;
  if (!matches) {
    containerWidth = props.containerWidth;
    contentWidth = props.contentWidth;
  }

  return (
    <div className={classes.background}>
      {/* <TwilioLogo className={classes.twilioLogo} /> */}
      {/* {user && location.pathname !== '/login' && <UserMenu />} */}
      <div className={classes.container}>
        <div className={classes.innerContainer} style={{ width: containerWidth }}>
          <div className={classes.swooshContainer}>
            <div className={classes.logoContainer}>
              {matches ? (
                <img className={classes.logo} src={LogoHorizontal} alt='Texas Health Department' />
              ) : (
                <img className={classes.logo} src={LogoVertical} alt='Texas Health Department' />
              )}
            </div>
          </div>
          <div className={classes.content} style={{ width: contentWidth }}>
            {props.children}
          </div>
        </div>
        {props.subContent && <div className={classes.subContentContainer}>{props.subContent}</div>}
      </div>
    </div>
  );
};

export default IntroContainer;
