import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContentText from '@material-ui/core/DialogContentText';
import { makeStyles, Theme } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    textAlign: 'center',
  },
  heading: {
    color: 'red',
    marginTop: '1em',
    marginBottom: '1em',
    fontWeight: 600,
  },
  button: {
    margin: '0 auto',
  },
}));

export default function SessionTimeout({ open, handleDisconnect }: any) {
  const classes = useStyles();

  return (
    <Dialog
      open={open}
      aria-labelledby='Session Timeout'
      aria-describedby='The room has timed out. Apologies, we were not able to connect now and we will try again later.'
      className={classes.container}
    >
      <DialogContent>
        <Typography variant='h5' className={classes.heading}>
          Session Timeout
        </Typography>
        <DialogContentText id='alert-dialog-description'>
          <Typography variant='body1'>
            The room has timed out. Apologies, we were not able to connect now and we will try again later.
          </Typography>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleDisconnect} color='primary' variant='contained' autoFocus className={classes.button}>
          Continue
        </Button>
      </DialogActions>
    </Dialog>
  );
}
