import React, { useEffect } from 'react';
import ParticipantList from '../ParticipantList/ParticipantList';
import { styled } from '@material-ui/core/styles';
import useVideoContext from '../../hooks/useVideoContext/useVideoContext';
import MainParticipant from '../MainParticipant/MainParticipant';
import useMainParticipant from '../../hooks/useMainParticipant/useMainParticipant';

import { TWILIO_RUNTIME_DOMAIN } from '../../constants';

import SessionTimeoutWarning from './SessionTimeoutWarning/SessionTimeoutWarning';
import SessionTimeout from './SessionTimeout/SessionTimeout';

const Container = styled('div')(({ theme }) => ({
  position: 'relative',
  height: '100%',
  display: 'grid',
  gridTemplateColumns: `1fr ${theme.sidebarWidth}px`,
  gridTemplateRows: '100%',
  [theme.breakpoints.down('sm')]: {
    gridTemplateColumns: `100%`,
    gridTemplateRows: `1fr ${theme.sidebarMobileHeight + 16}px`,
  },
}));

export default function Room() {
  const [open, setOpen] = React.useState(false);
  const [openWarning, setOpenWarning] = React.useState(false);
  const [warningTriggered, setWarningTriggered] = React.useState(false);

  const handleWarningAlert = () => {
    setOpenWarning(!openWarning);
  };

  const handleAlert = () => {
    setOpen(!open);
  };

  const mainParticipant = useMainParticipant();

  const {
    room: { localParticipant },
  } = useVideoContext();
  const isLocal = localParticipant === mainParticipant;

  const { room, taskId } = useVideoContext();

  const handleDisconnect = () => {
    const mapId = sessionStorage.getItem('mapId');
    const roomName = sessionStorage.getItem('roomName');

    fetch(`https://${TWILIO_RUNTIME_DOMAIN}/deletevideotask?taskId=${taskId}&mapId=${mapId}&key=${roomName}`)
      .then(res => res.json())
      .then(res => {
        sessionStorage.clear();
        window.history.replaceState(null, '', window.encodeURI(`/${window.location.search || ''}`));
        room.disconnect();
      })
      .catch(error => {
        sessionStorage.clear();
        handleAlert();
        window.history.replaceState(null, '', window.encodeURI(`/${window.location.search || ''}`));
      });
  };

  const onBeforeUnload = (e: any) => {
    // the method that will be used for both add and remove event
    e.preventDefault();
    window.history.replaceState(null, '', window.encodeURI(`/${window.location.search || ''}`));
    e.returnValue = 'Are you sure you want to leave room?';
    return 'Are you sure you want to leave room?';
  };

  useEffect(() => {
    let warningTimeout = setTimeout(() => {
      if (isLocal) {
        setOpenWarning(!openWarning);
        setWarningTriggered(true);
      }
    }, 60000);

    return () => clearTimeout(warningTimeout);
  }, [isLocal]);

  useEffect(() => {
    let timeout = setTimeout(() => {
      if (isLocal && warningTriggered) {
        setOpenWarning(false);
        setOpen(!open);
      }
    }, 120000);

    return () => clearTimeout(timeout);
  }, [warningTriggered, isLocal]);

  useEffect(() => {
    sessionStorage.setItem('isRoomJoined', 'true');
    window.addEventListener('beforeunload', onBeforeUnload);
  });

  return (
    <>
      <Container>
        <MainParticipant />
        <ParticipantList />
      </Container>

      <SessionTimeoutWarning open={openWarning} handleAlert={handleWarningAlert} />
      <SessionTimeout open={open} handleDisconnect={handleDisconnect} />
    </>
  );
}
