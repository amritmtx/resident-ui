import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContentText from '@material-ui/core/DialogContentText';
import { makeStyles, Theme } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Countdown from 'react-countdown';

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    textAlign: 'center',
  },
  heading: {
    color: 'red',
    marginTop: '1em',
    marginBottom: '1em',
    fontWeight: 600,
  },
  button: {
    margin: '0 auto',
  },
}));

export default function ExpirationWarningAlert({ open, handleAlert }: any) {
  const classes = useStyles();

  const renderer = ({ hours, minutes, seconds, completed }: any) => {
    if (completed) {
      // Render a complete state
      return <></>;
    } else {
      // Render a countdown
      return (
        <Typography variant='body1'>
          {minutes} minutes {seconds} seconds
        </Typography>
      );
    }
  };

  return (
    <Dialog
      open={open}
      onClose={handleAlert}
      aria-labelledby='Session Timeout Warning'
      aria-describedby='The session expires after (2) minutes of inactivity. If you wish to continue your session, please click Continue below.'
      className={classes.container}
    >
      <DialogContent>
        <Typography variant='h5' className={classes.heading}>
          Session Timeout Warning
        </Typography>
        <DialogContentText id='alert-dialog-description'>
          <Typography variant='body1'>
            The session expires after (2) minutes of inactivity. If you wish to continue your session, please click
            'Continue' below.
          </Typography>
        </DialogContentText>
        <Typography variant='body1'>Session automatically expires in:</Typography>
        <Countdown date={Date.now() + 120000} renderer={renderer} />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleAlert} color='primary' variant='contained' autoFocus className={classes.button}>
          Continue
        </Button>
      </DialogActions>
    </Dialog>
  );
}
