import React, { useState } from 'react';
// import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import { Button } from '@material-ui/core';
import { Redirect } from 'react-router-dom';

import ConfirmationAlert from './ConfirmationAlert/ConfirmationAlert';

import useVideoContext from '../../../hooks/useVideoContext/useVideoContext';

import { TWILIO_RUNTIME_DOMAIN } from '../../../constants';

// const useStyles = makeStyles((theme: Theme) =>
//   createStyles({
//     button: {
//       background: theme.brand,
//       color: 'white',
//       '&:hover': {
//         background: '#600101',
//       },
//     },
//   })
// );

export default function EndCallButton(props: { className?: string }) {
  const { room, taskId } = useVideoContext();
  const [redirect, setRedirect] = useState(false);
  const [open, setOpen] = useState(false);

  const handleAlert = () => {
    setOpen(!open);
  };

  const handleRoomDisconnect = () => {
    const mapId = sessionStorage.getItem('mapId');
    const roomName = sessionStorage.getItem('roomName');

    fetch(`https://${TWILIO_RUNTIME_DOMAIN}/deletevideotask?taskId=${taskId}&mapId=${mapId}&key=${roomName}`)
      .then(res => res.json())
      .then(res => {
        sessionStorage.clear();
        setRedirect(true);
        room.disconnect();
      })
      .catch(error => console.log(error));
  };

  if (!redirect) {
    return (
      <>
        <Button
          color='primary'
          variant='contained'
          onClick={handleAlert}
          className={props.className}
          data-cy-disconnect
        >
          Disconnect
        </Button>

        <ConfirmationAlert open={open} handleClose={handleAlert} handleDisconnect={handleRoomDisconnect} />
      </>
    );
  } else return <Redirect to='/completed' />;
}
