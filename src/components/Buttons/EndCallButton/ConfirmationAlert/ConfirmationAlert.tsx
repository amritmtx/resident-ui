import React from 'react';
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Button } from '@material-ui/core';

export default function SessionTimeout({ open, handleClose, handleDisconnect }: any) {
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby='Are you sure you want to disconnect?'
      aria-describedby='If you click Continue, then you will be disconnected from the video call.'
    >
      <DialogTitle id='alert-dialog-title'>Are you sure you want to disconnect?</DialogTitle>
      <DialogContent>
        <DialogContentText id='alert-dialog-description'>
          If you click 'Continue', then you will be disconnected from the video call.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button variant='contained' onClick={handleClose} color='primary'>
          Cancel
        </Button>
        <Button onClick={handleDisconnect} color='primary' autoFocus>
          Continue
        </Button>
      </DialogActions>
    </Dialog>
  );
}
