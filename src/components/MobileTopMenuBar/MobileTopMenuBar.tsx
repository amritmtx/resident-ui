import React, { useEffect } from 'react';
import { Grid, makeStyles, Theme, Typography } from '@material-ui/core';
// import useVideoContext from '../../hooks/useVideoContext/useVideoContext';
import EndCallButton from '../Buttons/EndCallButton/EndCallButton';
import Menu from '../MenuBar/Menu/Menu';
import Countdown from 'react-countdown';
import useVideoContext from '../../hooks/useVideoContext/useVideoContext';
import useMainParticipant from '../../hooks/useMainParticipant/useMainParticipant';

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    background: 'white',
    paddingLeft: '1em',
    display: 'none',
    height: `${theme.mobileTopBarHeight}px`,
    [theme.breakpoints.down('sm')]: {
      display: 'flex',
    },
  },
  endCallButton: {
    height: '28px',
    fontSize: '0.85rem',
    padding: '0 0.6em',
  },
  settingsButton: {
    [theme.breakpoints.down('sm')]: {
      height: '28px',
      minWidth: '28px',
      border: '1px solid rgb(136, 140, 142)',
      padding: 0,
      margin: '0 1em',
    },
  },
}));

export default function MobileTopMenuBar() {
  const classes = useStyles();
  // const { room } = useVideoContext();

  const [startCountdown, setStartCountdown] = React.useState(false);

  const mainParticipant = useMainParticipant();
  const {
    room: { localParticipant },
  } = useVideoContext();
  const isLocal = localParticipant === mainParticipant;

  const renderer = ({ hours, minutes, seconds, completed }: any) => {
    if (completed) {
      // Render a complete state
      return <></>;
    } else {
      // Render a countdown
      return (
        <Typography variant='body1'>
          {minutes} minutes {seconds} seconds
        </Typography>
      );
    }
  };

  useEffect(() => {
    let warningTimeout = setTimeout(() => {
      if (isLocal && !startCountdown) {
        setStartCountdown(true);
      }
      if (!isLocal) {
        setStartCountdown(false);
      }
    }, 60000);

    return () => clearTimeout(warningTimeout);
  }, [isLocal, startCountdown]);

  return (
    <Grid container alignItems='center' justify='space-between' className={classes.container}>
      <Grid style={{ flex: 1 }}>
        {startCountdown ? <Countdown date={Date.now() + 120000} renderer={renderer} /> : <></>}
      </Grid>
      <div>
        <EndCallButton className={classes.endCallButton} />
        <Menu buttonClassName={classes.settingsButton} />
      </div>
    </Grid>
  );
}
