import React, { useState, useEffect, FormEvent } from 'react';
import DeviceSelectionScreen from './DeviceSelectionScreen/DeviceSelectionScreen';
import IntroContainer from '../IntroContainer/IntroContainer';
import MediaErrorSnackbar from './MediaErrorSnackbar/MediaErrorSnackbar';
import PreflightTest from './PreflightTest/PreflightTest';
import RoomNameScreen from './RoomNameScreen/RoomNameScreen';
import { useAppState } from '../../state';
import { Redirect } from 'react-router-dom';
import useVideoContext from '../../hooks/useVideoContext/useVideoContext';
import Video from 'twilio-video';
import { TWILIO_RUNTIME_DOMAIN } from '../../constants';
import CircularProgress from '@material-ui/core/CircularProgress';

export enum Steps {
  roomNameStep,
  deviceSelectionStep,
}

export default function PreJoinScreens() {
  const { user } = useAppState();
  const { getAudioAndVideoTracks } = useVideoContext();
  const [step, setStep] = useState(Steps.roomNameStep);

  const [name, setName] = useState<string>(user?.displayName || '');
  const [roomName, setRoomName] = useState<string>('');
  const [redirect, setRedirect] = useState(false);

  const [mediaError, setMediaError] = useState<Error>();

  function getRoomName() {
    const match = window.location.search.match(/key=(.*)&?/);
    const key: any = match ? match[1] : window.sessionStorage.getItem('key');
    const params = new window.URLSearchParams({ key });
    return fetch(`https://${TWILIO_RUNTIME_DOMAIN}/getMapData?${params}`);
  }

  useEffect(() => {
    getRoomName()
      .then(res => res.json())
      .then((data: any) => {
        console.log(data);
        const expiryDate = new Date(data.dateExpiry).getTime();
        console.log(expiryDate - Date.now(), expiryDate < Date.now());
        if (expiryDate && expiryDate < Date.now()) {
          setRedirect(true);
        }
        sessionStorage.setItem('phone', data.phone);
        sessionStorage.setItem('ownerId', data.ownerId);
        sessionStorage.setItem('roomName', data.name);
        sessionStorage.setItem('mapId', data.mapId);
        sessionStorage.setItem('workerSid', data.workerSid);
        if (!data.name) setRedirect(true);
        setRoomName(data.name);
      })
      .catch(err => {
        setRedirect(true);
      });
  }, [roomName]);

  // useEffect(() => {
  //   if (URLRoomName) {
  //     setRoomName(URLRoomName);
  //     if (user?.displayName) {
  //       setStep(Steps.deviceSelectionStep);
  //     }
  //   }
  // }, [user, URLRoomName]);

  useEffect(() => {
    if (step === Steps.deviceSelectionStep) {
      getAudioAndVideoTracks().catch(error => {
        console.log('Error acquiring local media:');
        console.dir(error);
        setMediaError(error);
      });
    }
  }, [getAudioAndVideoTracks, step]);

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    // If this app is deployed as a twilio function, don't change the URL because routing isn't supported.
    if (!window.location.origin.includes('twil.io')) {
      window.history.replaceState(null, '', window.encodeURI(`/${window.location.search || ''}`));
    }
    setStep(Steps.deviceSelectionStep);
  };

  const SubContent = (
    <>
      {Video.testPreflight && <PreflightTest />}
      <MediaErrorSnackbar error={mediaError} />
    </>
  );
  if (!redirect) {
    if (roomName) {
      return (
        <>
          {step === Steps.roomNameStep && (
            <IntroContainer containerWidth='600px' contentWidth='300px'>
              <RoomNameScreen
                name={name}
                roomName={roomName}
                setName={setName}
                setRoomName={setRoomName}
                handleSubmit={handleSubmit}
              />
            </IntroContainer>
          )}

          {step === Steps.deviceSelectionStep && (
            <IntroContainer subContent={step === Steps.deviceSelectionStep && SubContent}>
              <DeviceSelectionScreen name={name} roomName={roomName} setStep={setStep} />
            </IntroContainer>
          )}
        </>
      );
    } else {
      return <CircularProgress style={{ position: 'fixed', top: '50%', left: '50%' }} />;
    }
  } else {
    return <Redirect to='/expiration-page' />;
  }
}
