import React from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/icons/Close';
import { createStyles, withStyles, Theme, makeStyles } from '@material-ui/core/styles';

const styles = (theme: Theme) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

const useStyles = makeStyles((theme: Theme) => ({
  note: {
    textAlign: 'justify',
  },
}));

export default function GuidelinesDialog({ open, handleDialog }: any) {
  const DialogTitle = withStyles(styles)((props: any) => {
    const { children, classes, onClose, ...other } = props;
    return (
      <MuiDialogTitle disableTypography className={classes.root} {...other}>
        <Typography variant='h6'>{children}</Typography>
        {onClose ? (
          <IconButton aria-label='close' className={classes.closeButton} onClick={onClose}>
            <CloseIcon />
          </IconButton>
        ) : null}
      </MuiDialogTitle>
    );
  });

  const classes = useStyles();

  return (
    <Dialog onClose={handleDialog} aria-labelledby='Guidelines' open={open}>
      <DialogTitle onClose={handleDialog}>Guidelines</DialogTitle>
      <DialogContent dividers>
        <Typography gutterBottom>Before your video visit, we recommend taking the following steps:</Typography>
        <Typography gutterBottom>
          <ol type='1'>
            <li>Find a quiet, private, and well-lit location to take the video call.</li>
            <li>Ensure your video device is adequately charged and connected to a strong Wi-Fi signal.</li>
            <li>
              Ensure your video device is stabilized on a stationary object and is far enough away from your face so
              that your signing can be fully viewed.
            </li>
            <li>Test your device audio, video, and web browser connections.</li>
          </ol>
        </Typography>
        <Typography variant='h6' style={{ textDecoration: 'underline' }} gutterBottom>
          Please note:
        </Typography>
        <Typography gutterBottom className={classes.note}>
          Information you tell the contact tracer will not be shared with your contacts. Your information will be kept
          completely private and used for public health purposes only. We will not ask you to verify your identity by
          providing a social security number, bank account, or credit card information. Contact tracing is a completely
          free service.
        </Typography>
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={handleDialog} color='primary' variant='contained'>
          Proceed
        </Button>
      </DialogActions>
    </Dialog>
  );
}
