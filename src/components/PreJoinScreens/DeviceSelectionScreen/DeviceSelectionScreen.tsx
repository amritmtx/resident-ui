import React from 'react';
import { makeStyles, Typography, Grid, Button, Theme, Hidden } from '@material-ui/core';
import LocalVideoPreview from './LocalVideoPreview/LocalVideoPreview';
import SettingsMenu from './SettingsMenu/SettingsMenu';
import { Steps } from '../PreJoinScreens';
import ToggleAudioButton from '../../Buttons/ToggleAudioButton/ToggleAudioButton';
import ToggleVideoButton from '../../Buttons/ToggleVideoButton/ToggleVideoButton';
import { useAppState } from '../../../state';
import useVideoContext from '../../../hooks/useVideoContext/useVideoContext';
import GuidelinesDialog from '../GuidelinesDialog/GuidelinesDialog';

import { TWILIO_RUNTIME_DOMAIN } from '../../../constants';

const useStyles = makeStyles((theme: Theme) => ({
  heading: {
    fontWeight: 600,
    marginBottom: '5px',
  },
  marginTop: {
    marginTop: '1em',
  },
  deviceButton: {
    width: '80%',
    border: '2px solid #aaa',
    margin: '1em 0',
  },
  localPreviewContainer: {
    paddingRight: '2em',
    [theme.breakpoints.down('sm')]: {
      padding: '0 2.5em',
    },
  },
  joinButtons: {
    display: 'flex',
    justifyContent: 'space-between',
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column-reverse',
      width: '100%',
      '& button': {
        margin: '0.5em 0',
      },
    },
  },
  mobileButtonBar: {
    [theme.breakpoints.down('sm')]: {
      display: 'flex',
      justifyContent: 'space-between',
      margin: '1.5em 0 1em',
    },
  },
  mobileButton: {
    padding: '0.8em 0',
    margin: 0,
  },
  note: {
    marginTop: '10px',
    marginBottom: '10px',
    textAlign: 'justify',
  },
}));
interface DeviceSelectionScreenProps {
  name: string;
  roomName: string;
  setStep: (step: Steps) => void;
}

export default function DeviceSelectionScreen({ name, roomName, setStep }: DeviceSelectionScreenProps) {
  const classes = useStyles();
  const { getToken, isFetching } = useAppState();
  const { connect, isAcquiringLocalTracks, isConnecting, setTaskId } = useVideoContext();

  const disableButtons = isFetching || isAcquiringLocalTracks || isConnecting;

  const [open, setOpen] = React.useState(true);

  const handleDialog = () => {
    setOpen(!open);
  };

  const onCreateTask = (taskRoomName: string, customerName: string) => {
    const mapId: any = sessionStorage.getItem('mapId');
    const phone: any = sessionStorage.getItem('phone');
    const ownerId: any = sessionStorage.getItem('ownerId');
    const workerSid: any = sessionStorage.getItem('workerSid');
    const params: any = new window.URLSearchParams({
      customerName: customerName,
      roomName: taskRoomName,
      phoneNumber: phone,
      mapId: mapId,
      ownerId: ownerId,
      worker: encodeURIComponent(localStorage.worker),
      workerSid: workerSid,
    });

    fetch(`https://${TWILIO_RUNTIME_DOMAIN}/createvideotask?${params}`)
      .then(res => res.json())
      .then(data => {
        setTaskId(data);
      })
      .catch(err => console.log(err));
  };

  const handleJoin = () => {
    getToken(name, roomName).then(token => {
      connect(token);
      if (sessionStorage.getItem('isRoomJoined') !== 'true') onCreateTask(roomName, name);
    });
  };

  return (
    <>
      <Typography variant='h5' className={classes.heading}>
        Hi {name}
      </Typography>
      <Typography className={classes.note}>
        Click 'Join Now' to initiate the video call with the Call Center Agent. After joining the video call, please
        wait and an Agent will join momentarily.
      </Typography>
      <Grid container justify='center'>
        <Grid item md={7} sm={12} xs={12}>
          <div className={classes.localPreviewContainer}>
            <LocalVideoPreview identity={name} />
          </div>
          <div className={classes.mobileButtonBar}>
            <Hidden mdUp>
              <ToggleAudioButton className={classes.mobileButton} disabled={disableButtons} />
              <ToggleVideoButton className={classes.mobileButton} disabled={disableButtons} />
            </Hidden>
            <SettingsMenu mobileButtonClass={classes.mobileButton} />
          </div>
        </Grid>
        <Grid item md={5} sm={12} xs={12}>
          <Grid container direction='column' justify='space-between' style={{ height: '100%' }}>
            <div>
              <Hidden smDown>
                <ToggleAudioButton className={classes.deviceButton} disabled={disableButtons} />
                <ToggleVideoButton className={classes.deviceButton} disabled={disableButtons} />
              </Hidden>
            </div>
            <div className={classes.joinButtons}>
              <Button variant='outlined' color='primary' onClick={() => setStep(Steps.roomNameStep)}>
                Cancel
              </Button>
              <Button
                variant='contained'
                color='primary'
                data-cy-join-now
                onClick={handleJoin}
                disabled={disableButtons}
              >
                Join Now
              </Button>
            </div>
          </Grid>
        </Grid>
      </Grid>
      <GuidelinesDialog open={open} handleDialog={handleDialog} />
    </>
  );
}
