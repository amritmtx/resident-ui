import React from 'react';
import IntroContainer from '../IntroContainer/IntroContainer';
import { Typography, makeStyles, Theme } from '@material-ui/core';
import CallEndIcon from '@material-ui/icons/CallEnd';

const useStyles = makeStyles((theme: Theme) => ({
  headingContainer: {
    marginTop: '3em',
    marginBottom: '2em',
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    marginTop: '1em',
    marginBottom: '1em',
    fontWeight: 600,
  },
  content: {
    marginBottom: '1em',
    fontWeight: 400,
    textAlign: 'justify',
  },
  logo: {
    width: '50px',
    height: '50px',
    marginRight: '15px',
    color: '#d3300a',
  },
}));

export default function WrapUpScreen() {
  const classes = useStyles();
  return (
    <IntroContainer containerWidth='600px' contentWidth='300px'>
      <div className={classes.headingContainer}>
        <CallEndIcon className={classes.logo} />
        <Typography variant='h5' className={classes.heading}>
          Call Ended
        </Typography>
      </div>
      <Typography className={classes.content}>Thank you for joining the video call.</Typography>
    </IntroContainer>
  );
}
