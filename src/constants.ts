export const DEFAULT_VIDEO_CONSTRAINTS: MediaStreamConstraints['video'] = {
  width: 1280,
  height: 720,
  frameRate: 24,
};

export const TWILIO_RUNTIME_DOMAIN = 'fandango-platypus-7667.twil.io';
